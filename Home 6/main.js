function Human (name,male,age){
    this.name = name;
    this.male = male;
    this.age = age;
}
function sortByAge(arr) {
    arr.sort((a, b) => a.age > b.age ? -1 : 1);
}
function age(array){
    array.sort((a,b) => a.age > b.age ? 1 : -1);
}

let Dima = new Human ("Dima", "man", 31);
let Svitlana = new Human ("Svitlana","female", 20);
let Igor = new Human ("Igor", "man", 29);
let Anna = new Human ("Anna","female", 27);

let arr = [Dima, Svitlana, Igor, Anna];
let array = [Dima,Svitlana,Igor,Anna];


sortByAge(arr);
age(array);

document.write(arr[0].name);
document.write(arr[1].name);
document.write(arr[2].name);
document.write(arr[3].name);
document.write ("<br><br>");
document.write(array[0].name);
document.write(array[1].name);
document.write(array[2].name);
document.write(array[3].name);
document.write ("<br><br>");

function Humann(name, surname,job){
    this.name = name;
    this.surname = surname;
    this.job = job;
    this.say = function () {
        document.write(`Hello! My name is ${this.name}, 
        and my surname is ${this.surname} 
        and i'm working in ${this.job} .`)
}
};
const vitalik = new Humann("Vitalik", "Chypchyruk", "University of Aviation");
vitalik.say();
